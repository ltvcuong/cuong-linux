# source env
function load_env() {
    scriptdir=`dirname "$BASH_SOURCE"`
    source $scriptdir/env.var 
}

##### FUNTIONS #####
function reload-bashrc() {
    source $ETC_DIR/bashrc.sh
}

function cdscuts_list_echo() {
    cat $1 | sed 's/#.*//g' | sed '/^\s*$/d'
}

function cdscuts_glob_echo() {
    system_wide_filelist=''
    user_filelist=''
 
    if [ -r $ETC_DIR/shell.bm.global ]; then
	system_wide_filelist=$(cdscuts_list_echo $ETC_DIR/shell.bm.global)
    fi
    if [ -r $ETC_DIR/shell.bm.user ]; then
	user_filelist=$(cdscuts_list_echo $ETC_DIR/shell.bm.user)
    fi
    
    echo -e "$system_wide_filelist\n$user_filelist" | sed '/^\s*$/d'
}

# source all files having name beginning with "eager"
function load_eagers() {
    for file in `find $ETC_DIR -iname "eager*"`
    do
	source $file
    done
}
##### PATH #####
PATH=$PATH:$JAVA_HOME/bin:$C_ALTERNATIVES:$MAVEN_HOME/bin:$NODEJS_HOME/bin:$WILDFLY_HOME/bin


load_env
load_eagers
